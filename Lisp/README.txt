Camilla Gussoni Matricola: 856679
Giacomo Lenzini Matricola: 851626

%------------------------------------%---------------------------------------%

Nome progetto: Graph Algorithms: Minimum Spanning Trees

%------------------------------------%---------------------------------------%

Obiettivo: 
Lo scopo di questo progetto è di implementare l’algoritmo di Prim per la 
soluzione del problema MST per grafi non-diretti e connessi con pesi non
negativi.


Conoscenze necessarie per la comprensione del progetto:
    -Conoscenze base di programmazione in linguaggio CommonLisp
    -Conoscenze grafi, heap, heapify
    -Algoritmo di Prim

%------------------------------------%---------------------------------------%

Progetto scolastico livello universitario a cura di:
    -Gussoni Camilla      
    -Lenzini Giacomo

%------------------------------------%---------------------------------------%

Mode: Lisp

%------------------------------------%---------------------------------------%
%------------------------------------%---------------------------------------%
    
    
INTERFACCIA CommonLisp PER LA REALIZZAZIONE DEI GRAFI:

is-graph graph-id → graph-id or NIL
Questa funzione ritorna il graph-id stesso se questo grafo è già stato creato, 
oppure NIL.


new-graph graph-id → graph-id
Questa funzione genera un nuovo grafo e lo inserisce nel data base (ovvero 
nella hash-table) dei grafi.


delete-graph graph-id → NIL
Rimuove l’intero grafo dal sistema (vertici archi etc); ovvero rimuove tutte 
le istanze presenti nei data base (ovvero nelle hash-tables) del sistema.


new-vertex graph-id vertex-id → vertex-rep
Aggiunge un nuovo vertice vertex-id al grafo graph-id.


delete-vertices graph-id → NIL
Questa funzione elimina tutti i vertici dal grafo graph-id.


graph-vertices graph-id → vertex-rep-list
Questa funzione torna una lista di vertici del grafo.


new-arc graph-id vertex-id vertex-id &optional weight → arc-rep
Questa funzione aggiunge un arco del grafo graph-id nella hash-table *arcs*. 


delete-arcs graph-id → NIL
Questa funzione elimina tutti glia archi dal grafo graph-id.


delete-arc graph-id vertex-id1 vertex-id2 weight → NIL
Questa funzione elimina l' arco bidirezionale dal grafo graph-id.


graph-arcs graph-id → arc-rep-list
Questa funzione ritorna una lista una lista di tutti gli archi presenti in 
graph-id.


graph-vertex-neighbors graph-id vertex-id → arc-rep-list
Questa funzione ritorna una lista arc-rep-list contenente gli archi
(arc graph-id vertex-id N W)
che portano ai vertici N immediatamente raggiungibili da vertex-id.


graph-vertex-adjacent graph-id vertex-id → vertex-rep-list
Questa funzione ritorna una lista vertex-rep-list contenente i vertici
(arc graph-id vertex-id V) adiacenti a vertex-id.


is-connected graph-id → boolean
Questa funzione restituisce t se il grafo è connesso, nil altrimenti.

graph-print graph-id
Questa funzione stampa alla console dell’interprete Common Lisp una lista dei 
vertici e degli archi del grafo graph-id.

%------------------------------------%---------------------------------------%

IMPLEMENTAZIONE Common Lisp SULLA REALIZZAZIONE DI UN MINHEAP:

new-heap H &optional (capacity 42) → heap-rep
Questa funzione inserisce un nuovo heap nella hash-table *heaps*.


heap-delete heap-id → T
Rimuove tutto lo heap indicizzato da heap-id.


heap-empty heap-id → boolean
Questa funzione è vero quando lo heap heap-id non contiene elementi.


heap-not-empty heap-id → boolean
Questa funzione è vero quando lo heap heap-id contiene almeno un elemento.


heap-full heap-id → boolean
Questa funzione è vero quando lo heap heap-id è pieno.


heap-head heap-id → (K V)
La funzione heap-head ritorna una lista di due elementi dove K è la chiave
minima e V il valore associato.


heap-insert heap-id K V → boolean
La funzione heap-insert inserisce l’elemento V nello heap heap-id con chiave K.


heap-insert-last-position heap-id K V → boolean
La funzione heap-insert inserisce l’elemento V nello heap heap-id con chiave K
in ultima posizione.


heap-insert-arcs heap-id arcs → boolean
Questa funzione è vero quando H è un heap e L una lista di archi nella forma 
arc(G, V, N, K) e inserisce ogni arco di L nello heap.


heap-extract heap-id → (K V)
La funzione heap-extract ritorna la lista con K, V e con K minima; la 
coppia è rimossa dallo heap heap-id. 


heap-modify-key heap-id new-key old-key V → boolean
La funzone heap-modify-key sostituisce la chiave OldKey (associata al valore V)
con NewKey. 


heap-find-key heap-id s key value → s
La funzone heap-find-key ritorna la posizione s dell' elemento nello heap  
con chiave K e valore V.


heap-size-increment heap-id → boolean
Questa funzione aumenta di 1 la dimensione dello heap.


heap-size-decrement heap-id → boolean
Questa funzione decrementa di 1 la dimensione dello heap.


heap-member array s value  → boolean
Questa funzione è vero quando l' elemento con valore value è presente 
nell' array in posizione s.


heap-print heap-id → boolean
Questa funzione stampa sulla console lo stato interno dello heap heap-id.


heap-swap heap-id x y → boolean
Questa funzione scambia x con y nello heap heap-id.


heap-heapify heap-id p → boolean
Questa funzione verifica che l' elemento in posizione N nello heap-id
è minore dei sui figli; in caso contrario scambia il padre con il minimo 
tra i suoi figli per mantenere la struttura di min-heap.


heap-ctrl heap-id p → boolean
Questa funzione verifica che l' elemento in posizione N nello heap-id sia 
maggiore del padre e, in caso contrario, scambia padre e figlio.


minimum heap-id x y
Questa funzione restituisce l' elemento minimo tra x e y nello heap-id.

%------------------------------------%---------------------------------------%


IMPLEMENTAZIONE CommonLisp SULLA REALIZZAZIONE DI UN MST 
TRAMITE L'UTILIZZO DELL'ALGORITMO DI PRIM:

mst-vertex-key graph-id vertex-id → k
Questa funzione, dato un vertex-id di un grafo graph-id ritorna, durante e 
dopo l’esecuzione dell’algoritmo di Prim, il peso minimo di un arco che 
connette vertex-id nell’albero minimo; se questo arco non esiste 
(ed all’inizio dell’esecuzione) allora k è MOST-POSITIVE-DOUBLE-FLOAT.


mst-previous graph-id V → U
Questa funzione, durante e dopo l’esecuzione dell’algoritmo di Prim, ritorna 
il vertice U che il vertice “genitore” (“precedente”, o “parent”) di V nel 
minimum spanning tree V.


mst-vertex-root graph-id → v
Questa funzione ritorna il valore della radice.


set-previous graph-id vertex-id U → boolean
Questa funzione assegna il previous di vertex-id a U.


set-vertex-key graph-id vertex-id K → boolean
Questa funzione assegna la chiave K al vertice vertex-id.


set-vertex-key-root graph-id vertex-id → boolean
Questa funzione assegna la chiave 0 al vertice vertex-id.


set-vertex-key graph-id vertex-id → boolean
Questa funzione assegna la chiave MOST-POSITIVE-DOUBLE-FLOAT al vertice 
vertex-id.


is-visited graph-id vertex-id → boolean
Questa funzione ritorna t se vertex-id è stato già visitato, nil altrimenti.


add-visited graph-id vertex-id → boolean
Questa funzione aggiunge il vertice vertex-id ai vertici già visitati.


mst-arcs graph-id arcs → arc-rep-list
Questa funzione, data una lista arcs di archi nella forma arc(G, V, N, K) 
crea una lista arc-rep-list contenente archi appartenenti a arcs 
e che non finiscono in vertici già visitati.


mst-arcs-key graph-id arcs → boolean
Data una lista arcs nella forma arc(G, V, N, K),
questa funzione assegna chiave K al vertice N (vertex_key(G, N, K)
e vertex_previous(G, N, V) nel momento in cui
K è minore o uguale della chiave già assegnata a N.
Altrimenti.


findall-arcs graph-id source → arcs
Questa funzione crea una lista arcs contente tutti gli archi figli di source
nel MST ordinati rispetto al peso dell' arco e se peso uguale secondo 
l' ordine lessicografico.


mst-prim graph-id source → NIL
Questa funzione termina con un effetto collaterale. 
Dopo la sua esecuzione, la hash-table *vertex-key* contiene al suo interno le 
associazioni (graph-id V) ⇒ d per ogni V appartenente a graph-id;
la hash-table *previous* contiene le associazioni (graph-id V) ⇒ U calcolate 
durante l’esecuzione dell’algoritmo di Prim.


mst-get graph-id source → preorder-mst
Questa funzione ritorna preorder-mst che è una lista degli archi del MST 
ordinata secondo un attraversamento preorder dello stesso, fatta rispetto
al peso dell’arco.


mst-get-2 graph-id vertex-id arcs → preorder-mst
Questa funzione ritorna preorder-mst che è una lista degli archi del MST 
ordinata secondo un attraversamento preorder dello stesso, fatta rispetto
al peso dell’arco.
