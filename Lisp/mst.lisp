;;;; -*- Mode : Lisp -*-


;;; Camilla Gussoni Matricola: 856679
;;; Giacomo Lenzini Matricola: 851626


;;; hash-tables

(defparameter *vertices* (make-hash-table :test #'equal))

(defparameter *arcs* (make-hash-table :test #'equal))

(defparameter *graphs* (make-hash-table :test #'equal))

(defparameter *visited* (make-hash-table :test #'equal))

(defparameter *vertex-keys* (make-hash-table :test #'equal))

(defparameter *previous* (make-hash-table :test #'equal))


;;; GRAPH

;;; is-graph
(defun is-graph (graph-id)
  (gethash graph-id *graphs*))


;;; new-graph
(defun new-graph (graph-id)
  (or (gethash graph-id *graphs*)
      (setf (gethash graph-id *graphs*) graph-id)))


;;; delete-vertices
(defun delete-vertices (graph-id)
  (maphash (lambda (k v)
             (cond
              ((eql (second k) graph-id)
               (remhash k *vertices*)
               (eql v "")))) *vertices*))

;;; delete-arcs
(defun delete-arcs (graph-id)
  (maphash (lambda (k v)
             (cond
              ((eql (second k) graph-id)
               (remhash k *arcs*)
               (eql v "")))) *arcs*))


;;; delete-arc
(defun delete-arc (graph-id vertex-id1 vertex-id2 k)
  (cond
   ((and (gethash graph-id *graphs*)
         (not (eql vertex-id1 vertex-id2))
         (gethash (list 'vertex-rep graph-id vertex-id1) *vertices*)
         (gethash (list 'vertex-rep graph-id vertex-id2) *vertices*)
         (gethash (list 'arc-rep graph-id vertex-id1 vertex-id2 k) *arcs*)
         (gethash (list 'arc-rep graph-id vertex-id2 vertex-id1 k) *arcs*))
    (remhash (list 'arc-rep graph-id vertex-id1 vertex-id2 k) *arcs*)
    (remhash (list 'arc-rep graph-id vertex-id2 vertex-id1 k) *arcs*))))


;;; delete-graph
(defun delete-graph (graph-id)
  (remhash graph-id *graphs*)
  (delete-vertices graph-id)
  (delete-arcs graph-id)
  t)


;;; new-vertex
(defun new-vertex (graph-id vertex-id)
  (setf (gethash (list 'vertex-rep graph-id vertex-id) *vertices*)
        (list 'vertex-rep graph-id vertex-id)))


;;; graph-vertices
(defun graph-vertices (graph-id)
  (let ((vertex-rep-list ())
        (values ()))
    (maphash (lambda (k v)
               (cond
                ((eql (second k) graph-id)
                 (push k vertex-rep-list)
                 (push v values)))) *vertices*) vertex-rep-list))


;;; new-arc
(defun new-arc (graph-id vertex-id1 vertex-id2 &optional (weight 1))
  (cond
   ((and (gethash graph-id *graphs*)
         (not (eql vertex-id1 vertex-id2))
    (gethash (list 'vertex-rep graph-id vertex-id1) *vertices*)
    (gethash (list 'vertex-rep graph-id vertex-id2) *vertices*))
    (maphash (lambda (k v)
               (cond
                ((and
                  (eql (second k) graph-id)
                  (eql (third k) vertex-id1)
                  (eql (fourth k) vertex-id2)
                  (delete-arc graph-id vertex-id1 vertex-id2 (fifth k))
                  (eql v "")))))
             *arcs*)
    (setf (gethash (list 'arc-rep graph-id vertex-id2 vertex-id1 weight)
                   *arcs*)
          (list 'arc-rep graph-id vertex-id2 vertex-id1 weight))
    (setf (gethash (list 'arc-rep graph-id vertex-id1 vertex-id2 weight)
                   *arcs*)
          (list 'arc-rep graph-id vertex-id1 vertex-id2 weight)))))


;;; graph-arcs
(defun graph-arcs (graph-id)
  (let (( arc-rep-list ())
	      (values ()))
    (maphash (lambda (k v)
               (cond
		            ((eql (second k) graph-id)
                 (push k arc-rep-list)
                 (push v values))))
             *arcs*)
    arc-rep-list))


;;; graph-vertex-neighbors
(defun graph-vertex-neighbors (graph-id vertex-id)
  (let ((arc-rep-list ())
        (values ()))
    (maphash (lambda (k v)
               (cond
                ((and (eql (second k) graph-id) (eql (third k) vertex-id))
                 (push k arc-rep-list)
                 (push v values))))
             *arcs*)
    arc-rep-list))


;;; graph-vertex-adjacent
(defun graph-vertex-adjacent (graph-id vertex-id)
  (let ((vertex-rep-list ())
        (values ()))
    (maphash (lambda (k v)
               (cond
                ((and (eql (second k) graph-id) (eql (third k) vertex-id))
                 (push (fourth k) vertex-rep-list)
                 (push v values))))
             *arcs*)
    vertex-rep-list))


;;; is-connected graph-id
(defun is-connected (graph-id)
  (let ((tmp 0))
    (maphash (lambda (k v)
               (cond
                ((eql (graph-vertex-adjacent graph-id (third k)) nil)
                 (setf tmp 1)
                 (eql v ""))))
             *vertices*)
    (cond
     ((eql tmp 1) nil)
     ((eql tmp 0) t))))


;;; graph-print
(defun graph-print (graph-id)
  (let ((v (graph-vertices graph-id))
        (a (graph-arcs graph-id)))
    (print 'Vertex) (print v)
    (terpri)
    (print 'Arcs) (print a)))


;;; HEAP
(defparameter *heaps* (make-hash-table :test #'equal))

;;; new-heap
(defun new-heap (heap-id &optional (capacity 42))
  (or (gethash heap-id *heaps*)
      (setf (gethash heap-id *heaps*)
            (list 'heap heap-id 0 (make-array capacity)))))

;;; heap-id
(defun heap-id (heap-id)
  (let ((l (gethash heap-id *heaps*)))
    (second l)))


;;; heap-size
(defun heap-size (heap-id)
  (let ((l (gethash heap-id *heaps*)))
    (third l)))


;;; heap-actual-heap
(defun heap-actual-heap (heap-id)
  (let ((l (gethash heap-id *heaps*)))
    (fourth l)))


;;; heap-delete
(defun heap-delete (heap-id)
  (remhash heap-id *heaps*))


;;; heap-empty
(defun heap-empty (heap-id)
  (let ((l (gethash heap-id *heaps*)))
    (cond ((= (third l) 0) t))))


;;; heap-not-empty
(defun heap-not-empty (heap-id)
  (let ((l (gethash heap-id *heaps*)))
    (cond ((/= (third l) 0) t))))


;;; heap-full
(defun heap-full (heap-id)
  (let ((l (gethash heap-id *heaps*)))
    (cond
     ((eql (length (fourth l)) (heap-size heap-id)) t))))


;;; heap-head
(defun heap-head (heap-id)
  (aref (heap-actual-heap heap-id) 0))


;;; heap-swap
(defun heap-swap (heap-id x y)
  (let (( h (heap-actual-heap heap-id)))
    (let ((c (aref h y)))
        (setf (aref h y) (aref h x))
        (setf (aref h x) c))))


;;; heap-size-increment
(defun heap-size-increment (heap-id)
  (let ((l (gethash heap-id *heaps*)))
    (setf (third l) (+ (third l) 1))))


;;; heap-size-decrement
(defun heap-size-decrement (heap-id)
  (let ((l (gethash heap-id *heaps*)))
    (setf (third l) (- (third l) 1))))


;;; heap-member
(defun heap-member (array s value)
  (cond
   ((< s 0) nil)
   (t
    (let ((k (first (aref array s)))
          (v (second (aref array s))))
      (cond
       ((eql v value) k)
       (t (heap-member array (- s 1) value)))))))


;;; minimum
(defun minimum (heap-id x y)
  (let ((h (heap-actual-heap heap-id)))
    (let ((k1 (first (aref h x)))
            (k2 (first (aref h y)))
          (v1 (second (aref h x)))
          (v2 (second (aref h y))))
      (cond
       ((< k1 k2) x)
       ((> k1 k2) y)
       ((= k1 k2)
        (cond
         ((not (numberp v1))
          (cond
           ((string< v1 v2) x)
           ((string> v1 v2) y)))
         ((numberp v1)
          (cond
           ((< v1 v2) x)
           ((> v1 v2) y)))))))))


;;; heap-ctrl
(defun heap-ctrl (heap-id p)
  (let ((s (heap-size heap-id)))
    (cond
     ((heap-empty heap-id) nil)
     ((heap-not-empty heap-id)
      (cond
       ((eql s 1) t)
       ((= p 0) t)
       ((and (not (= s 1)) (not (= p 0)))
        (let ((father (floor (/ p 2))))
          (let ((m (minimum heap-id father p)))
            (cond
             ((= m father))
             ((not (= m father))
              (heap-swap heap-id m father)
              (heap-ctrl heap-id father) t))))))))))


;;; heap-heapify
(defun heap-heapify (heap-id p)
  (let ((s (heap-size heap-id))
        (n (+ p 1)))
    (cond
     ((heap-empty heap-id) nil)
     ((heap-not-empty heap-id)
      (cond
       ((eql s 1) t)
       ((< s n) nil)
       ((<= n s)
        (let ((left (* 2 n))
              (right (+ (* 2 n) 1)))
          (cond
           ((and (> left s)
                 (> right s))
            nil)
           ((and (<= left s)
                 (> right s))
            (let ((m (minimum heap-id (- left 1) p)))
              (cond
               ((= m p))
               ((not (= m p))
                (heap-swap heap-id m p)))))
           ((and (<= left s)
                 (<= right s))
            (let ((m (minimum heap-id
                              (minimum heap-id (- left 1) p)
                              (- right 1))))
              (cond
               ((= m p))
               ((not (= m p))
                (heap-swap heap-id m p)
                (heap-heapify heap-id m))))))) t))))))


;;; heap-find-key
(defun heap-find-key (array s key value)
  (cond
   ((< s 0) nil)
   (t
    (let ((k (first (aref array s)))
          (v (second (aref array s))))
      (cond
       ((and (= k key) (eql v value)) s)
       (t (heap-find-key array (- s 1) key value)))))))


;;; heap-modify-key
(defun heap-modify-key (heap-id new-key old-key V)
  (let ((l (heap-actual-heap heap-id))
  (e (heap-find-key (heap-actual-heap heap-id)
            (- (heap-size heap-id) 1) old-key V)))
    (cond
     ((eql e nil) nil)
     (t
      (let ( (v (second (aref l e))))
        (cond
         ((< new-key (first (aref l e)))
	        (setf (aref l e) (list new-key v))
	        (heap-ctrl heap-id e))
         ((= new-key (first (aref l e))))
         ((> new-key (first (aref l e)))
          (setf (aref l e) (list new-key v))
	        (heap-heapify heap-id e))))))))


;;; heap-insert-last-position
(defun heap-insert-last-position (heap-id K V)
  (let ((l (gethash heap-id *heaps*))
        (s (heap-size heap-id)))
    (setf (aref (fourth l) s) (list K V))
    (heap-size-increment heap-id) t))


;;; heap-insert
(defun heap-insert (heap-id K V)
  (cond
   ((heap-empty heap-id)
    (heap-insert-last-position heap-id K V))
   ((and (not (heap-full heap-id)) (heap-not-empty heap-id))
    (let ((e (heap-member
              (heap-actual-heap heap-id)
              (- (heap-size heap-id) 1) V)))
      (cond
       ((eql e NIL)
        (heap-insert-last-position heap-id K V)
        (heap-ctrl heap-id (- (heap-size heap-id) 1)))
       ((not (eql e NIL))
        (cond
         ((> K e))
         ((<= K e)
          (heap-modify-key heap-id K e V)))))))))


;;; heap-extract
(defun heap-extract (heap-id)
  (cond
   ((heap-empty heap-id) nil)
   (t
    (heap-swap heap-id 0 (- (heap-size heap-id) 1))
    (setf (aref (heap-actual-heap heap-id) (- (heap-size heap-id) 1)) 0)
    (heap-size-decrement heap-id)
    (heap-heapify heap-id 0))))


;;; heap-insert-arcs
(defun heap-insert-arcs (heap-id arcs)
  (let ((l (car arcs)))
     (cond
      ((eql l NIL) nil)
      (t
       (heap-insert heap-id (fifth l) (fourth l))
       (heap-insert-arcs heap-id (remove l arcs))))))


;;; heap-print
(defun heap-print (heap-id)
  (print (gethash heap-id *heaps*)) t)


;;; MST

;;; mst-vertex-key
(defun mst-vertex-key (graph-id vertex-id)
  (let ((w ())
        (values ()))
    (maphash (lambda (k v)
               (cond
                ((and (eql (second k) graph-id) (eql (third k) vertex-id))
                 (push (fourth k) w)
                 (push v values))))
             *vertex-keys*)
    (car w)))


;;; mst-previous
(defun mst-previous (graph-id V)
  (let ((w ())
        (values ()))
    (maphash (lambda (k z)
               (cond
                ((and (eql (second k) graph-id) (eql (third k) V))
                 (push (fourth k) w)
                 (push z values))))
             *previous*)
    (car w)))


;;; set-previous
(defun set-previous (graph-id vertex-id U)
  (remhash
   (gethash
    (list 'previous graph-id vertex-id (mst-previous graph-id vertex-id))
            *previous*)
   *previous*)
  (setf (gethash (list 'previous graph-id vertex-id U)
                 *previous*)
        (list 'previous graph-id vertex-id U)))


;;; set-vertet-key
(defun set-vertex-key (graph-id vertex-id k)
  (remhash
   (gethash
    (list 'vertex-key graph-id vertex-id (mst-vertex-key graph-id vertex-id))
            *vertex-keys*)
   *vertex-keys*)
  (setf (gethash (list 'vertex-key graph-id vertex-id k)
                 *vertex-keys*)
        (list 'vertex-key graph-id vertex-id k)))


;;; mst-vertex-key-root
(defun mst-vertex-root (graph-id)
  (let ((w ())
        (values ()))
    (maphash (lambda (k v)
               (cond
                ((and
                  (eql (second k) graph-id)
                  (eql (fourth k) 0))
                 (push (third k) w)
                 (push v values))))
             *vertex-keys*)
    (car w)))


;;; set-vertet-key-root
(defun set-vertex-key-root (graph-id vertex-id)
  (setf (gethash (list 'vertex-key graph-id vertex-id 0)
            *vertex-keys*)
        (list 'vertex-key graph-id vertex-id 0)))


;;; set-vertet-key-inf
(defun set-vertex-key-inf (graph-id vertex-id)
  (setf
   (gethash (list 'vertex-key graph-id vertex-id MOST-POSITIVE-DOUBLE-FLOAT)
            *vertex-keys*)
        (list 'vertex-key graph-id vertex-id MOST-POSITIVE-DOUBLE-FLOAT)))


;;; is-visited
(defun is-visited (graph-id vertex-id)
  (let ((visited (gethash (list 'visited graph-id vertex-id) *visited*)))
    visited))


;;; mst-arcs
(defun mst-arcs (graph-id arcs)
  (let ((arc-rep-list ())
        (values ()))
    (maphash (lambda (k v)
               (cond
                ((and
                  (member k arcs)
                  (not (is-visited graph-id (fourth k))))
                 (push k arc-rep-list)
                 (push v values)))) *arcs*)
    (cond
     ((numberp (third (car arc-rep-list)))
      (let ((sorted-arcs (sort arc-rep-list #'< :key #'fourth)))
        (stable-sort sorted-arcs #'< :key #'fifth)))
     ((not (numberp (third (car arc-rep-list))))
      (let ((sorted-arcs (sort arc-rep-list #'string< :key #'fourth)))
        (stable-sort sorted-arcs #'< :key #'fifth))))))


;;; add-visited
(defun add-visited (graph-id vertex-id)
  (setf (gethash (list 'visited graph-id vertex-id) *visited*)
        (list 'visited graph-id vertex-id)))


;;; mst-arcs-key
(defun mst-arcs-key (graph-id arcs)
  (cond
   ((eql arcs nil) nil)
   (t
    (let ((l (car arcs)))
      (and (gethash graph-id *graphs*)
           (gethash (list 'vertex-rep graph-id (third l)) *vertices*)
           (gethash (list 'vertex-rep graph-id (fourth l)) *vertices*)
           (gethash l *arcs*))
      (let ((key (mst-vertex-key graph-id (fourth l)))
            (new-key (fifth l))
            (vertex (fourth l))
            (source (third l)))
        (cond
         ((>= key new-key)
          (set-vertex-key graph-id vertex new-key)
          (set-previous graph-id vertex source)
          (mst-arcs-key graph-id (remove l arcs)))
         ((< key new-key)
          (mst-arcs-key graph-id (remove l arcs)))))))) t)


;;; findall-arcs
(defun findall-arcs (graph-id source)
  (let ((arc-rep-list ())
        (values ()))
    (maphash (lambda (k v)
               (cond
                ((and (eql (second k) graph-id)
                      (eql (third k) source)
                      (eql (mst-previous graph-id (fourth k)) source)
                      (eql (mst-vertex-key graph-id (fourth k)) (fifth k)))
                 (push k arc-rep-list) (push v values)))) *arcs*)
    (cond
     ((numberp source)
      (let ((sorted-arcs (sort arc-rep-list #'< :key #'fourth)))
        (stable-sort sorted-arcs #'< :key #'fifth)))
     ((not (numberp source))
      (let ((sorted-arcs (sort arc-rep-list #'string< :key #'fourth)))
        (stable-sort sorted-arcs #'< :key #'fifth))))))


;;; mst-prim
(defun mst-prim (graph-id source)
  (cond
   ((and (gethash graph-id *graphs*)
         (gethash (list 'vertex-rep graph-id source) *vertices*))
    (cond
     ((= (hash-table-count *visited*) 0)
      (cond
       ((eql (hash-table-count *vertices*) 1)
        (set-vertex-key-root graph-id source) t)
       ((not (eql (hash-table-count *vertices*) 1))
        (add-visited graph-id source)
        (set-vertex-key-root graph-id source)
        (maphash (lambda (k v)
                   (cond
                    ((and
                      (eql (second k) graph-id)
                      (not (eql (third k) source)))
                     (set-vertex-key-inf graph-id (third k))
                     (eql v "")))) *vertices*)
        (cond
         ((is-connected graph-id)
          (let ((arcs
                 (mst-arcs graph-id (graph-vertex-neighbors graph-id source))))
            (mst-arcs-key graph-id arcs)
            (new-heap graph-id (hash-table-count *vertices*))
            (heap-insert-arcs graph-id arcs)
            (let ((head (heap-head graph-id)))
              (heap-extract graph-id)
              (add-visited graph-id (second head))
              (mst-prim graph-id (second head)))))
         ((not (is-connected graph-id)) nil)))))
     ((> (hash-table-count *visited*) 0)
      (cond
       ((eql (hash-table-count *vertices*) (hash-table-count *visited*)) t)
       ((not (eql (hash-table-count *vertices*) (hash-table-count *visited*)))
        (let ((arcs
               (mst-arcs graph-id (graph-vertex-neighbors graph-id source))))
          (mst-arcs-key graph-id arcs)
          (heap-insert-arcs graph-id arcs)
          (let ((head (heap-head graph-id)))
            (heap-extract graph-id)
            (add-visited graph-id (second head))
            (mst-prim graph-id (second head)))))))))))


;;; mst-get-2
(defun mst-get-2 (graph-id vertex-id arcs)
  (let ((arc (car arcs)))
    (let ((arcs2 (findall-arcs graph-id (fourth arc)))
          (empty ()))
      (cond
       ((eql (length arcs) 0)
        empty)
       ((eql (length arcs) 1)
        (let ((k (mst-get-2 graph-id (fourth arc) arcs2)))
          (append arcs k)))
       ((and (not (eql (length arcs) 0)) (not(eql (length arcs) 1)))
        (let ((k (mst-get-2 graph-id (fourth arc) arcs2)))
          (let ((p (append (list arc) k)))
            (let ((sub-arcs (remove arc arcs)))
              (let ((l (mst-get-2 graph-id vertex-id sub-arcs)))
                (append p l))))))))))


;;; mst-get
(defun mst-get (graph-id source)
  (cond
   ((and (gethash graph-id *graphs*)
         (gethash (list 'vertex-rep graph-id source) *vertices*))
    (let ((arcs (findall-arcs graph-id source)))
      (cond
       ((eql (length arcs) 0) (list ()))
       (t
        (mst-get-2 graph-id source arcs)))))))

;;;;; -*- End of file -*-
