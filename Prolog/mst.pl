%%%% -*- Mode : Prolog -*-

%%% Camilla Gussoni Matricola: 856679
%%% Giacomo Lenzini Matricola: 851626


%%% Grafi
:- dynamic graph/1.

%%% Vertici
:- dynamic vertex/2.

%%% Archi
:- dynamic arc/4.

%%% new_graph/1
new_graph(G) :- graph(G), !.
new_graph(G) :- assert(graph(G)), !.


%%% delete_graph/1
delete_graph(G) :-
    retract(graph(G)),
    retractall(vertex(G, _)),
    retractall(arc(G, _, _, _)).


%%% new_vertex/2
new_vertex(G, V) :- vertex(G, V), !.
new_vertex(G, V) :- assert(vertex(G, V)), !.


%%% graph_vertices/2
graph_vertices(G, Vs) :- findall(V, vertex(G, V), Vs), !.


%%% list_vertices/1
list_vertices(G) :-
    graph(G),
    listing(vertex(G, _)).


%%% new_arc/4
new_arc(G, U, V, Weight) :-
    arc(G, U, V, _),
    retract(arc(G, U, V, _)),
    retract(arc(G, V, U, _)),
    assert(arc(G, U, V, Weight)),
    assert(arc(G, V, U, Weight)), !.

new_arc(G, U, V, Weight) :-
    \+ arc(G, U, V, _),
    assert(arc(G, U, V, Weight)),
    assert(arc(G, V, U, Weight)), !.


%%% new_arc/3
new_arc(G, U, V) :-  new_arc(G, U, V, 1), !.


%%% graph_arcs/2
graph_arcs(G, Es) :-
    graph(G),
    findall(arc(G, V, N, 1), arc(G, V, N, 1), Es), !.


%%% vertex_neighbors/3
vertex_neighbors(G, V, Ns) :-
    graph(G),
    vertex(G, V),
    findall(arc(G, V, N, K), arc(G, V, N, K), Ns), !.


%%% adjs/3
adjs(G, V, Vs) :-
    graph(G),
    vertex(G, V),
    findall(V1, arc(G, V, V1, 1), Vs), !.


%%% list_arcs/1
list_arcs(G) :-
    graph(G),
    listing(arc(G, _, _, _)), !.


%%% list_graph/1
list_graph(G) :-
    graph(G),
    list_vertices(G),
    list_arcs(G), !.


%%% read_graph/2
read_graph(G, FileName) :-
    \+ graph(G),
    new_graph(G),
    csv_read_file(FileName, Arcs, [separator(0'\t), arity(3)]),
    create_arcs(G, Arcs), !.

read_graph(G, FileName) :-
    graph(G),
    delete_graph(G),
    new_graph(G),
    csv_read_file(FileName, Arcs, [separator(0'\t), arity(3)]),
    create_arcs(G, Arcs), !.


%%% create_arcs/2
create_arcs(_, []).
create_arcs(G, [S]) :-
    arg(1, S, X),
    arg(2, S, Y),
    arg(3, S, Z),
    new_vertex(G, X),
    new_vertex(G, Y),
    new_arc(G, X, Y, Z), !.
create_arcs(G, [S | T]) :-
    arg(1, S, X),
    arg(2, S, Y),
    arg(3, S, Z),
    new_vertex(G, X),
    new_vertex(G, Y),
    new_arc(G, X, Y, Z),
    create_arcs(G, T).


%%% write_graph/3
write_graph(G, FileName, Type) :-
    graph(G),
    Type = graph,
    !,
    arcs(G, L),
    csv_write_file(FileName, L, [separator(0'\t)]).
write_graph(G, FileName, Type) :-
    is_list(G),
    verified(G),
    Type = edges,
    !,
    csv_write_file(FileName, G, [separator(0'\t)]).


%%% arcs/2
arcs(G, L) :- findall(arc(G, V, N, W), arc(G, V, N, W), L).


%%% write_graph/2
write_graph(G, FileName) :-
    write_graph(G, FileName, graph).


%%% verified/1
verified([]) :- !.
verified([H | T]) :- H = arc(_, _, _, _), verified(T).


%%% HEAP
:- dynamic heap/2.
:- dynamic heap_entry/4.

%%% new_heap/1
new_heap(H) :- heap(H, _S), !.
new_heap(H) :- assert(heap(H, 0)), !.


%%% delete_heap/1
delete_heap(H) :-
    heap(H, _),
    retractall(heap_entry(H, _, _, _)),
    retract(heap(H, _)), !.


%%% heap_has_size/2
heap_has_size(H, S) :- heap(H, S), !.


%%% heap_empty/1
heap_empty(H) :- heap(H, 0), !.


%%% heap_not_empty/1
heap_not_empty(H) :- heap(H, S), S \= 0, !.


%%% heap_head/3
heap_head(H, _, _) :-
    heap(H, _),
    heap_empty(H), !.

heap_head(H, K, V) :-
    heap(H, _),
    heap_not_empty(H),
    heap_entry(H, 1, K, V), !.


%%% heap_insert/3
heap_insert(H, K, V) :-
    heap(H, _),
    heap_not_empty(H),
    heap_entry(H, _, K, V), !.
heap_insert(H, K, V) :-
    heap(H, _),
    heap_not_empty(H),
    heap_entry(H, _, OldKey, V),
    K > OldKey, !.
heap_insert(H, K, V) :-
    heap(H, _),
    heap_not_empty(H),
    heap_entry(H, _, OldKey, V),
    K =< OldKey,
    modify_key(H, K, OldKey, V), !.
heap_insert(H, K, V) :-
    heap(H, S),
    X is S+1,
    \+ heap_entry(H, _, K, V),
    retract(heap(H, S)),
    assert(heap(H, X)),
    assert(heap_entry(H, X, K, V)),
    ctrl(H, X), !.


%%% heap_insert_arcs/2
heap_insert_arcs(_, []).
heap_insert_arcs(G, [arc(G, _, V, K)]) :-
    heap(G, _),
    heap_insert(G, K, V), !.
heap_insert_arcs(G, [arc(G, _, V, K) | T]) :-
    heap(G, _),
    heap_insert(G, K, V),
    heap_insert_arcs(G, T), !.


%%% heap_extract/3
heap_extract(H, _, _) :-
    heap_empty(H), !.
heap_extract(H, K, V) :-
    heap(H, _),
    heap_not_empty(H),
    \+ heap_head(H, K, V), !.
heap_extract(H, K, V) :-
    heap(H, S),
    heap_not_empty(H),
    S = 1,
    heap_entry(H, S, K, V),
    X is S-1,
    retract(heap_entry(H, S, K, V)),
    retract(heap(H, S)),
    assert(heap(H, X)), !.
heap_extract(H, K, V) :-
    heap(H, S),
    heap_not_empty(H),
    S \= 1,
    X is S-1,
    heap_head(H, K, V),
    heap_entry(H, S, K1, V1),
    switch(H, 1, K, V, S, K1, V1),
    retract(heap_entry(H, S, K, V)),
    retract(heap(H, S)),
    assert(heap(H, X)),
    heapify(H, 1), !.


%%% modify_key/4
modify_key(H, NewKey, OldKey, V) :-
    heap(H, _),
    NewKey = OldKey,
    heap_entry(H, _, NewKey, V), !.
modify_key(H, NewKey, OldKey, V) :-
    heap(H, _),
    heap_entry(H, X, OldKey, V),
    NewKey < OldKey,
    retract(heap_entry(H, X, OldKey, V)),
    assert(heap_entry(H, X, NewKey, V)),
    ctrl(H, X), !.
modify_key(H, NewKey, OldKey, V) :-
    heap(H, _),
    heap_entry(H, X, OldKey, V),
    NewKey > OldKey,
    retract(heap_entry(H, X, OldKey, V)),
    assert(heap_entry(H, X, NewKey, V)),
    heapify(H, X), !.


%%% list_heap/1
list_heap(H) :-
    heap(H, _),
    listing(heap_entry(H, _, _, _)).


%%% heapify/2
heapify(H, _) :-
    heap_empty(H), !.
heapify(H, _) :-
    heap_not_empty(H),
    heap(H, 1), !.
heapify(H, N) :-
    heap_not_empty(H),
    heap(H, S),
    S < N, !.
heapify(H, N) :-
    heap_not_empty(H),
    heap(H, S),
    S \= 0,
    S \= 1,
    N =< S,
    L is 2*N,
    L > S,
    R is ((2 * N) + 1),
    R > S, !.
heapify(H, N) :-
    heap_not_empty(H),
    heap(H, S),
    S \= 0,
    S \= 1,
    N =< S,
    L is 2*N,
    L =< S,
    R is ((2 * N) + 1),
    R > S,
    heap_entry(H, L, _, _),
    heap_entry(H, N, _, _),
    min(H, N, L, M),
    N = M,
    !.
heapify(H, N) :-
    heap_not_empty(H),
    heap(H, S),
    S \= 0,
    S \= 1,
    N =< S,
    L is 2*N,
    L =< S,
    R is ((2 * N) + 1),
    R > S,
    heap_entry(H, L, _, _),
    heap_entry(H, N, _, _),
    min(H, N, L, M),
    N \= M,
    heap_entry(H, M, Km, Vm),
    heap_entry(H, N, Kn, Vn),
    switch(H, M, Km, Vm, N, Kn, Vn),
    heapify(H, M), !.
heapify(H, N) :-
    heap_not_empty(H),
    heap(H, S),
    S \= 0,
    S \= 1,
    N =< S,
    L is 2*N,
    L =< S,
    R is ((2 * N) + 1),
    R =< S,
    heap_entry(H, L, _, _),
    heap_entry(H, R, _, _),
    heap_entry(H, N, _, _),
    min(H, N, L, M1),
    min(H, M1, R, M),
    N = M, !.
heapify(H, N) :-
    heap_not_empty(H), heap(H, S),
    S \= 0,
    S \= 1,
    N =< S,
    L is 2*N,
    L =< S,
    R is ((2 * N) + 1),
    R =< S,
    heap_entry(H, L, _, _),
    heap_entry(H, R, _, _),
    heap_entry(H, N, _, _),
    min(H, N, L, M1),
    min(H, M1, R, M),
    N \= M,
    heap_entry(H, M, Km, Vm),
    heap_entry(H, N, Kn, Vn),
    switch(H, M, Km, Vm, N, Kn, Vn),
    heapify(H, M), !.


%%% ctrl/2
ctrl(H, _) :-
    heap(H, _), heap_empty(H), !.
ctrl(H, _) :-
    heap(H, S), heap_not_empty(H), S = 1, !.
ctrl(H, N) :-
    heap(H, S), heap_not_empty(H), S \= 1, N = 1, !.
ctrl(H, N) :-
    heap(H, S), heap_not_empty(H), S \= 1, N \= 1,
    heap_entry(H, N, _, _),
    F is floor(N/2),
    heap_entry(H, F, _, _),
    min(H, N, F, M), M = F, !.
ctrl(H, N) :-
    heap(H, S), heap_not_empty(H), S \= 1, N \= 1,
    heap_entry(H, N, Kn, Vn),
    F is floor(N/2),
    heap_entry(H, F, Kf, Vf),
    min(H, N, F, M),
    M \= F,
    switch(H, N, Kn, Vn, F, Kf, Vf),
    ctrl(H, F), !.


%%% switch/1
switch(H, X, K1, V1, Y, K2, V2) :-
    heap_entry(H, X, K1, V1),
    heap_entry(H, Y, K2, V2),
    retract(heap_entry(H, X, K1, V1)),
    retract(heap_entry(H, Y, K2, V2)),
    assert(heap_entry(H, Y, K1, V1)),
    assert(heap_entry(H, X, K2, V2)), !.


%%% min/4
min(H, P1, P2, M) :-
    heap(H, S),
    P1 =< S,
    P2 =< S,
    heap_entry(H, P1, K1, _),
    heap_entry(H, P2, K2, _),
    K1 < K2, M is P1, !.

min(H, P1, P2, M) :-
    heap(H, S),
    P1 =< S,
    P2 =< S,
    heap_entry(H, P1, K1, V1),
    heap_entry(H, P2, K2, V2),
    K1 = K2, V1 @< V2, M is P1, !.

min(H, P1, P2, M) :-
    heap(H, S),
    P1 =< S,
    P2 =< S,
    heap_entry(H, P1, K1, V1),
    heap_entry(H, P2, K2, V2),
    K1 = K2, V1 @> V2, M is P2, !.

min(H, P1, P2, M) :-
    heap(H, S),
    P1 =< S,
    P2 =< S,
    heap_entry(H, P1, K1, _),
    heap_entry(H, P2, K2, _),
    K1 > K2, M is P2, !.


%%% Algoritmo di PRIM

%%% vertex_key
:- dynamic vertex_key/3.
%%% vertex_previous
:- dynamic vertex_previous/3.

%%% initialize_root(G, V).
initialize_root(G, V) :-
    vertex(G, V),
    K is 0,
    assert(vertex_key(G, V, K)), !.


%%% set_vKey_inf/2
set_vKey_inf(G, V) :-
    vertex(G, V),
    K is inf,
    assert(vertex_key(G, V, K)), !.


%%% initialize_key/2
initialize_key(G, [V]) :-
    set_vKey_inf(G, V), !.
initialize_key(G, [V | Vk]) :-
    set_vKey_inf(G, V),
    initialize_key(G, Vk), !.


%%% vertex_to_list/3
vertex_to_list(G, L, Root) :-
    graph(G),
    findall(V, vertex(G, V), Lk), delete(Lk, Root, L), !.


%%% add_visited/4
add_visited(G, V, L, Vi) :-
    graph(G), vertex(G, V),
    append([V], L, Vi).


%%% arcs_list/4
arcs_list(G, Ln, Vi, L) :-
    graph(G),
    findall(arc(G, V, S, W), (member(arc(G, V, S, W), Ln), \+member(S, Vi)), L1),
    sort(3, @<, L1, L2),
    sort(4, @=<, L2, L),
    !.


%%% mst_arcs_key/2
mst_arcs_key(_, []).
mst_arcs_key(G, [arc(G, S, N, K)]) :-
    graph(G),
    vertex(G, S),
    vertex(G, N),
    arc(G, S, N, K),
    vertex_key(G, N, W),
    vertex_previous(G, N, _),
    W >= K,
    retract(vertex_key(G, N, W)),
    assert(vertex_key(G, N, K)),
    retract(vertex_previous(G, N, _)),
    assert(vertex_previous(G, N, S)),
    !.

mst_arcs_key(G, [arc(G, S, N, K)]) :-
    graph(G),
    vertex(G, S),
    vertex(G, N),
    arc(G, S, N, K),
    vertex_key(G, N, W),
    \+vertex_previous(G, N, _),
    W >= K,
    retract(vertex_key(G, N, W)),
    assert(vertex_key(G, N, K)),
    assert(vertex_previous(G, N, S)),
    !.

mst_arcs_key(G, [arc(G, S, N, K)]) :-
    graph(G),
    vertex(G, S),
    vertex(G, N),
    arc(G, S, N, K),
    vertex_key(G, N, W),
    W < K,
    !.

mst_arcs_key(G, [arc(G, S, N, K) | T]) :-
    graph(G),
    vertex(G, S),
    vertex(G, N),
    arc(G, S, N, K),
    vertex_key(G, N, W),
    vertex_previous(G, N, _),
    W >= K,
    retract(vertex_key(G, N, W)),
    assert(vertex_key(G, N, K)),
    retract(vertex_previous(G, N, _)),
    assert(vertex_previous(G, N, S)),
    mst_arcs_key(G, T), !.

mst_arcs_key(G, [arc(G, S, N, K) | T]) :-
    graph(G),
    vertex(G, S),
    vertex(G, N),
    arc(G, S, N, K),
    vertex_key(G, N, W),
    \+vertex_previous(G, N, _),
    W >= K,
    retract(vertex_key(G, N, W)),
    assert(vertex_key(G, N, K)),
    assert(vertex_previous(G, N, S)),
    mst_arcs_key(G, T), !.


mst_arcs_key(G, [arc(G, S, N, K) | T]) :-
    graph(G),
    vertex(G, S),
    vertex(G, N),
    arc(G, S, N, K),
    vertex_key(G, N, W),
    W < K,
    mst_arcs_key(G, T), !.


%%%  mst_prim/2
mst_prim(G, Source) :-
    graph(G), vertex(G, Source),
    findall(N, vertex(G, N), Vg),
    length(Vg, X), X = 1,
    initialize_root(G, Source),
    \+ arc(G, Source, _, _), !.

mst_prim(G, Source) :-
    graph(G), vertex(G, Source),
    vertex_neighbors(G, Source, L),
    initialize_root(G, Source),
    vertex_to_list(G, [S | Vk], Source),
    initialize_key(G, [S | Vk]),
    new_heap(G),
    mst_arcs_key(G, L),
    heap_insert_arcs(G, L),
    heap_head(G, Ks, Vs),
    heap_extract(G, Ks, Vs),
    add_visited(G, Vs, [], Vi),
    add_visited(G, Source, Vi, Vi2),
    mst_prim_2(G, Vs, Vi2),
    !.


%%% mst_prim_2/2
mst_prim_2(G, Source, Vi) :-
    graph(G), vertex(G, Source),
    findall(N, vertex(G, N), Vg), length(Vi, X), length(Vg, Y), X = Y,
    !.

mst_prim_2(G, Source, Vi) :-
    graph(G), vertex(G, Source),
    vertex_neighbors(G, Source, L1), arcs_list(G, L1, Vi, L),
    findall(N, vertex(G, N), Vg), length(Vi, X), length(Vg, Y), X \= Y,
    mst_arcs_key(G, L),
    heap_insert_arcs(G, L),
    heap_head(G, Ks, Vs),
    heap_extract(G, Ks, Vs),
    add_visited(G, Vs, Vi, Vi2),
    vertex_previous(G, Vs, N),
    mst_prim_2(G, Vs, Vi2),
    !.


%%% mst_get/3
mst_get(G, Source, PreorderTree):-
    graph(G), vertex(G, Source),
    findall(arc(G, Source, S, K), (vertex_previous(G, S, Source), vertex_key(G, S, K)), PreorderTree), PreorderTree = [], !.
mst_get(G, Source, PreorderTree) :-
    graph(G), vertex(G, Source),
    findall(arc(G, Source, S, K), (vertex_previous(G, S, Source), vertex_key(G, S, K)), PT),
    sort(3, @<, PT, PT1),
    sort(4, @=<, PT1, PTf),
    mst_get(G, Source, PTf, PreorderTree), !.


%%% mst_get/4
mst_get(_, _, [], L) :- L = [], !.
mst_get(G, Source, [arc(G, Source, V, K)], L) :-
    graph(G), vertex(G, Source), vertex(G, V),
    findall(arc(G, V, S, W), (vertex_previous(G, S, V), vertex_key(G, S, W)), PT),
    sort(3, @<, PT, PT1),
    sort(4, @=<, PT1, PTf),
    mst_get(G, V, PTf, L1),
    append([arc(G, Source, V, K)], L1, L), !.
mst_get(G, Source, [arc(G, Source, V, K) | T], L) :-
    graph(G), vertex(G, Source), vertex(G, V),
    findall(arc(G, V, S, W), (vertex_previous(G, S, V), vertex_key(G, S, W)), PT),
    sort(3, @<, PT, PT1),
    sort(4, @=<, PT1, PTf),
    mst_get(G, V, PTf, L1),
    append([arc(G, Source, V, K)], L1, L2),
    mst_get(G, Source, T, L3),
    append(L2, L3, L), !.


%%%% -*- End of file -*-
