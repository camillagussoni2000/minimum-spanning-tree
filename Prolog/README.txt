Camilla Gussoni Matricola: 856679
Giacomo Lenzini Matricola: 851626

%------------------------------------%---------------------------------------%

Nome progetto: Graph Algorithms: Minimum Spanning Trees

%------------------------------------%---------------------------------------%

Obiettivo: 
Lo scopo di questo progetto è di implementare l’algoritmo di Prim per la 
soluzione del problema MST per grafi non-diretti e connessi con pesi non 
negativi.

Conoscenze necessarie per la comprensione del progetto:
    -Conoscenze base di programmazione in linguaggio Prolog
    -Conoscenze grafi, heap, heapify
    -Algoritmo di Prim

%------------------------------------%---------------------------------------%

Progetto scolastico livello universitario a cura di:
    -Gussoni Camilla      
    -Lenzini Giacomo

%------------------------------------%---------------------------------------%

Mode: Prolog    
    
%------------------------------------%---------------------------------------%
%------------------------------------%---------------------------------------%
    

INTERFACCIA Prolog PER LA REALIZZAZIONE DI GRAFI:

:- new_graph(G).
Questo predicato inserisce un nuovo grafo nella base-dati Prolog. 


:- delete_graph(G).
Rimuove tutto il grafo (vertici e archi inclusi) dalla base-dati Prolog.


:- new_vertex(G, V).
Aggiunge il vertice V nella base-dati Prolog. N.B. si richiede che il 
predicato che rappresenta i vertici, da aggiungere alla base-dati Prolog, 
sia vertex(G, V). 


:- graph_vertices (G, Vs).
Questo predicato è vero quando Vs è una lista contenente tutti i vertici di G.


:- list_vertices(G).
Questo predicato stampa alla console dell’interprete Prolog una lista dei 
vertici del grafo G.


:- new_arc(G, U, V, Weight).
Aggiunge un arco del grafo G alla base dati Prolog.


:- new_arc(G, U, V).
Aggiugne un arco di peso 1 del grafo G alla base dati Prolog.


:- graph_arcs(G, Es).
Questo predicato è vero quando Es è una lista di tutti gli archi 
presenti in G.


:- vertex_neighbors(G, V, Ns).
Questo predicato è vero quando V è un vertice di G e Ns è una lista 
contenente gli archi, arc(G, V, N, W) , che portano ai vertici N 
immediatamente raggiungibili da V.


:- adjs(G, V, Vs).
Questo predicato è vero quando V è un vertice di G e Vs è una lista contenente 
i vertici, vertex(G, V), ad esso adiacenti; si noti che in un grafo non diretto 
si devono inserire nella lista Vs tutti i vertici adiacenti.


:- list_arcs(G).
Questo predicato stampa alla console dell’interprete Prolog una lista degli 
archi del grafo G.


:- list_graph(G).
Questo predicato stampa alla console dell’interprete Prolog una lista dei 
vertici e degli archi del grafo G.


:- read_graph(G).
Questo predicato legge un “grafo” G, da un file FileName e lo inserisce nel 
data base di Prolog. Il formato di questo file è semplicissimo. Ogni riga 
contiene 3 elementi separati da un carattere di tabulazione.
Il tipo di file deve essere ".csv".


:- create_arcs(G, Arcs).
Questo predicato è vero quando G è un grafo e Arcs è una lista contenente 
archi nella forma (V, N, K), dove V e N sono vertici e K il peso dell' arco.
Il predicato crea vertici e arco nel grafo G per ogni arco della lista Arcs.


:- write_graph(G, FileName).
:- write_graph(G, FileName, Type).
Questo predicato è vero quando G viene scritto sul file FileName secondo il 
valore dell’argomento Type. Type può essere graph o edges. Se Type è graph, 
allora G è un termine che identifica un grafo nella base di dati Prolog; 
In FileName saranno scritti gli archi del grafo secondo il formato descitto 
per read_graph/2. Se Type è edges, allora G è una lista di archi, ognuno dei 
quali viene stampato su FileName, sempre secondo il formato descritto
per read_graph/2.


:- arcs(G, L).
Questo predicato è vero quando G è un grafo e L è la lista degli archi di G.


:- verified(L).
Questo predicato è vero quando L è una lista contenente solo archi nella forma 
arc(G, V, N, K).

%------------------------------------%---------------------------------------%

IMPLEMENTAZIONE Prolog SULLA REALIZZAZIONE DI UN MINHEAP: 

:- new_heap(H).
Questo predicato inserisce un nuovo heap nella base-dati Prolog.


:- delete_heap(H).
Rimuove tutto lo heap (incluse tutte le “entries”) dalla base-dati Prolog.


:- heap_has_size(H, S).
Questo predicato è vero quanto S è la dimensione corrente dello heap. 


:- heap_empty(H).
Questo predicato è vero quando lo heap H non contiene elementi.


:- heap_not_empty(H).
Questo predicato è vero quando lo heap H contiene almeno un elemento.


:- heap_head(H, K, V).
Il predicato head/3 è vero quando l’elemento dello heap H con 
chiave minima K è V.


:- heap_insert(H, K, V).
Il predicato insert/3 è vero quando l’elemento V è inserito nello heap H 
con chiave K.
Naturalmente, lo heap H dovrà essere ristrutturato in modo da mantenere 
la "heap property" per ogni nodo.


:- heap_insert_arcs(H, L).
Questo predicato è vero quando H è un heap e L una lista di archi nella 
forma arc(G, V, N, K) e inserisce ogni arco di L nello heap.


:- heap_extract(H, K, V).
Il predicato extract/3 è vero quando la coppia K, V con K minima, è rimossa
dallo heap H. Naturalmente, lo heap H dovrà essere ristrutturato in modo 
da mantenere la “heap property” per ogni nodo.


:- modify_key(H, NewKey, OldKey, V).
Il predicato modify_key/4 è vero quando la chiave OldKey 
(associata al valore V) è sostituita da NewKey.


:- list_heap(H).
Il predicato richiama listing/1 per stampare sulla console Prolog lo stato
interno dello heap.


:- heapify(H, N).
Questo predicato è vero quando H è un heap e verifica che l' elemento in 
posizione N è minore dei sui figli; in caso contrario scambia il padre con 
il minimo tra i suoi figli per mantenere la struttura di min-heap.


:- ctrl(H, S).
Questo predicato è vero quando H è un heap e verifica che l' elemento in 
posizione N sia maggiore del padre e, in caso contrario, scambia padre e 
figlio.


:- switch(H, X, K1, V1, Y, K2, V2).
Questo predicato scambia l' heap_entry(H, X, K1, V1) con 
heap_entry(H, Y, K2, V2).


:- min(H, P1, P2, M).
Questo predicato è vero quando H è un heap e M è il minimo tra l' elemento 
in posizione P1 e l' elemento in posizione P2 in H.


%------------------------------------%---------------------------------------%

IMPLEMENTAZIONE Prolog SULLA REALIZZAZIONE DI UN MST TRAMITE L'USO
DELL'ALGORITMO DI PRIM:

:- vertex_key(G, V, K).
Questo predicato è vero quando V è un vertice di G e, durante e dopo 
l’esecuzione dell’algoritmo di Prim, contiene il peso minimo di un arco che 
connette V nell’albero minimo; se questo arco non esiste (ed all’inizio 
dell’esecuzione) allora K è inf.
Questo predicato va dichiarato dynamic.


:- vertex_previous(G, V, U).
Questo predicato è vero quando V ed U sono vertici di G e, durante e dopo 
l’esecuzione dell’algoritmo di Prim, il vertice U è il vertice “genitore” 
(“precedente”, o “parent”) di V nel minimum spanning tree.
Questo predicato va dichiarato dynamic.


:- initialize_root(G, V).
Questo predicato è vero quando G è un grafo e V un vertice a cui assegna 
chiave K=0.


:- set_vKey_inf(G, V).
Questo predicato è vero quando G è un grafo e V un vertice a cui assegna 
chiave K=inf.


:- initialize_key(G, L).
Questo predicato è vero quando G è un grafo e L è una lista contente tutti 
i vertici del grafo meno la radice e assegna ai vertici di L chiave K=inf.


:- vertex_to_list(G, L, Root).
Questo predicato è vero quando G è un grafo e L la lista contente tutti i 
vertici di G meno la radice Root.


:- add_visited(G, V, L, Vi).
Questo predicato è vero quando G è un grafo, V un vertice di G.
Aggiunge il vertice V alla lista L creando la nuova lista Vi.


:- arcs_list(G, Ln, Vi, L).
Questo predicato è vero quando G è un grafo e L è una lista contenente 
tutti gli archi uscenti che appartengono alla lista Ln e che non finiscono 
in un vertici appartenente alla lista dei vertici già visitati Vi.


:- mst_arcs_key(G, L).
Questo predicato è vero quando G è un grafo e L una lista di archi nella forma 
arc(G, V, N, K). Assegna chiave K al vertice N (vertex_key(G, N, K) e 
vertex_previous(G, N, V) nel momento in cui K è minore o uguale della chiave 
già assegnata a N. Altrimenti.

%------------------------------------%---------------------------------------%
:- mst_prim(G, Source).
Questo predicato ha successo con un effetto collaterale. Dopo la sua prova, 
la base-dati Prolog ha al suo interno i predicati vertex_key(G, V, k) per 
ogni V appartenente a G; la base-dati Prolog contiene anche i predicati 
vertex_previous(G, V, U) per ogni V, ottenuti durante le iterazioni
dell’algoritmo di Prim.


:- mst_prim_2(G, Source, Vi).
Questo predicato ha successo con un effetto collaterale. Dopo la sua prova, l
a base-dati Prolog ha al suo interno i predicati vertex_key(G, V, k) per ogni V 
appartenente a G; la base-dati Prolog contiene anche i predicati 
vertex_previous(G, V, U) per ogni V, ottenuti durante le iterazioni
dell’algoritmo di Prim.


:- mst_get(G, Source, PreorderTree)
Questo predicato è vero quando PreorderTree è una lista degli archi del MST 
ordinata secondo un attraversamento preorder dello stesso, fatta rispetto al 
peso dell’arco e se peso uguale secondo l' ordine lessicografico.


:- mst_get(G, Source, PTf, L)
Questo predicato è vero quando PTf è la lista di archi figli di Source di MST 
e L è una lista degli archi del MST ordinata secondo un attraversamento 
preorder dello stesso, fatta rispetto al peso dell’arco e se peso uguale 
secondo l' ordine lessicografico.
